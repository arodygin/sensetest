$(function() {

    var $body = $('body');

    $('#reset').click(function() {
        window.location = 'touch.html';
    });

    $('#back').click(function() {
        window.location = 'index.html';
    });

    setTimeout(function() {
        $body.one('keyup', function() {
            var timerStart = new Date();
            $body.one('keyup', function() {
                var timerStop = new Date();
                var timer = (timerStop.getTime() - timerStart.getTime()) / 1000;
                $('#content').html(timer.toFixed(3) + ' sec');
                $('button').show();
            });
        });
    }, 100);
});
