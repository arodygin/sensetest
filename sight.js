$(function() {

    var $body = $('body');
    var delay = Math.floor((Math.random() * 4000) + 1000);

    $('#reset').click(function() {
        window.location = 'sight.html';
    });

    $('#back').click(function() {
        window.location = 'index.html';
    });

    setTimeout(function() {
        $body.removeClass('light').addClass('bright');
        var timerStart = new Date();
        $body.one('keypress', function() {
            var timerStop = new Date();
            var timer = (timerStop.getTime() - timerStart.getTime()) / 1000;
            $body.removeClass('bright').addClass('light');
            $('#content').html(timer.toFixed(3) + ' sec');
            $('button').show();
        });
    }, delay);
});
